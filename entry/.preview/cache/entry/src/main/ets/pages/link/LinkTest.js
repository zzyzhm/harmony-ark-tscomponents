"use strict";
class GreenButtonState {
    constructor(width) {
        this.width = 0;
        this.width = width;
    }
}
class GreenButton extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__greenButtonState = new SynchedPropertyObjectTwoWayPU(params.greenButtonState, this, "greenButtonState");
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__greenButtonState.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__greenButtonState.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get greenButtonState() {
        return this.__greenButtonState.get();
    }
    set greenButtonState(newValue) {
        this.__greenButtonState.set(newValue);
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Button.createWithLabel('Green Button');
            Button.debugLine("pages/link/LinkTest.ets(14:5)");
            Button.width(this.greenButtonState.width);
            Button.height(40);
            Button.backgroundColor('#64bb5c');
            Button.fontColor('#FFFFFF，90%');
            Button.onClick(() => {
                if (this.greenButtonState.width < 700) {
                    // 更新class的属性，变化可以被观察到同步回父组件
                    this.greenButtonState.width += 60;
                }
                else {
                    // 更新class，变化可以被观察到同步回父组件
                    this.greenButtonState = new GreenButtonState(180);
                }
            });
            if (!isInitialRender) {
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Button.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
class YellowButton extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__yellowButtonState = new SynchedPropertySimpleTwoWayPU(params.yellowButtonState, this, "yellowButtonState");
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__yellowButtonState.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__yellowButtonState.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get yellowButtonState() {
        return this.__yellowButtonState.get();
    }
    set yellowButtonState(newValue) {
        this.__yellowButtonState.set(newValue);
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Button.createWithLabel('Yellow Button');
            Button.debugLine("pages/link/LinkTest.ets(36:5)");
            Button.width(this.yellowButtonState);
            Button.height(40);
            Button.backgroundColor('#f7ce00');
            Button.fontColor('#FFFFFF，90%');
            Button.onClick(() => {
                // 子组件的简单类型可以同步回父组件
                this.yellowButtonState += 40.0;
            });
            if (!isInitialRender) {
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Button.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
class ShufflingContainer extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__greenButtonState = new ObservedPropertyObjectPU(new GreenButtonState(180), this, "greenButtonState");
        this.__yellowButtonProp = new ObservedPropertySimplePU(180, this, "yellowButtonProp");
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.greenButtonState !== undefined) {
            this.greenButtonState = params.greenButtonState;
        }
        if (params.yellowButtonProp !== undefined) {
            this.yellowButtonProp = params.yellowButtonProp;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__greenButtonState.purgeDependencyOnElmtId(rmElmtId);
        this.__yellowButtonProp.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__greenButtonState.aboutToBeDeleted();
        this.__yellowButtonProp.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get greenButtonState() {
        return this.__greenButtonState.get();
    }
    set greenButtonState(newValue) {
        this.__greenButtonState.set(newValue);
    }
    get yellowButtonProp() {
        return this.__yellowButtonProp.get();
    }
    set yellowButtonProp(newValue) {
        this.__yellowButtonProp.set(newValue);
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("pages/link/LinkTest.ets(55:5)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({ direction: FlexDirection.Column, alignItems: ItemAlign.Center });
            Flex.debugLine("pages/link/LinkTest.ets(56:7)");
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // 简单类型从父组件@State向子组件@Link数据同步
            Button.createWithLabel('Parent View: Set yellowButton');
            Button.debugLine("pages/link/LinkTest.ets(58:9)");
            // 简单类型从父组件@State向子组件@Link数据同步
            Button.width(312);
            // 简单类型从父组件@State向子组件@Link数据同步
            Button.height(40);
            // 简单类型从父组件@State向子组件@Link数据同步
            Button.margin(12);
            // 简单类型从父组件@State向子组件@Link数据同步
            Button.fontColor('#FFFFFF，90%');
            // 简单类型从父组件@State向子组件@Link数据同步
            Button.onClick(() => {
                this.yellowButtonProp = (this.yellowButtonProp < 700) ? this.yellowButtonProp + 40 : 100;
            });
            if (!isInitialRender) {
                // 简单类型从父组件@State向子组件@Link数据同步
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        // 简单类型从父组件@State向子组件@Link数据同步
        Button.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // class类型从父组件@State向子组件@Link数据同步
            Button.createWithLabel('Parent View: Set GreenButton');
            Button.debugLine("pages/link/LinkTest.ets(67:9)");
            // class类型从父组件@State向子组件@Link数据同步
            Button.width(312);
            // class类型从父组件@State向子组件@Link数据同步
            Button.height(40);
            // class类型从父组件@State向子组件@Link数据同步
            Button.margin(12);
            // class类型从父组件@State向子组件@Link数据同步
            Button.fontColor('#FFFFFF，90%');
            // class类型从父组件@State向子组件@Link数据同步
            Button.onClick(() => {
                this.greenButtonState.width = (this.greenButtonState.width < 700) ? this.greenButtonState.width + 100 : 100;
            });
            if (!isInitialRender) {
                // class类型从父组件@State向子组件@Link数据同步
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        // class类型从父组件@State向子组件@Link数据同步
        Button.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            __Common__.create();
            __Common__.margin(12);
            if (!isInitialRender) {
                __Common__.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new 
                    // class类型初始化@Link
                    GreenButton(this, { greenButtonState: this.__greenButtonState }, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        __Common__.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            __Common__.create();
            __Common__.margin(12);
            if (!isInitialRender) {
                __Common__.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new 
                    // 简单类型初始化@Link
                    YellowButton(this, { yellowButtonState: this.__yellowButtonProp }, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        __Common__.pop();
        Flex.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new ShufflingContainer(undefined, {}));
ViewStackProcessor.StopGetAccessRecording();
//# sourceMappingURL=LinkTest.js.map