"use strict";
AppStorage.SetOrCreate('PropA', 47);
let storage = new LocalStorage({ 'PropA': 100 });
class CompA extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__localStorLink = this.createLocalStorageLink("PropA", 1, "localStorLink");
        this.__storLink = this.createStorageLink('PropA', 1, "storLink");
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        this.__storLink.aboutToBeDeleted();
        this.__localStorLink.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get storLink() {
        return this.__storLink.get();
    }
    set storLink(newValue) {
        this.__storLink.set(newValue);
    }
    get localStorLink() {
        return this.__localStorLink.get();
    }
    set localStorLink(newValue) {
        this.__localStorLink.set(newValue);
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create({ space: 20 });
            Column.debugLine("pages/AppStorage/Test2.ets(11:5)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(`From AppStorage ${this.storLink}`);
            Text.debugLine("pages/AppStorage/Test2.ets(12:7)");
            Text.onClick(() => this.storLink += 1);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(`From LocalStorage ${this.localStorLink}`);
            Text.debugLine("pages/AppStorage/Test2.ets(15:7)");
            Text.onClick(() => this.localStorLink += 1);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new CompA(undefined, {}, storage));
ViewStackProcessor.StopGetAccessRecording();
//# sourceMappingURL=Test2.js.map