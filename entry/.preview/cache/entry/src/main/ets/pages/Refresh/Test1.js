"use strict";
class RefreshExample extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__isRefreshing = new ObservedPropertySimplePU(false, this, "isRefreshing");
        this.__counter = new ObservedPropertySimplePU(0, this, "counter");
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.isRefreshing !== undefined) {
            this.isRefreshing = params.isRefreshing;
        }
        if (params.counter !== undefined) {
            this.counter = params.counter;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__isRefreshing.purgeDependencyOnElmtId(rmElmtId);
        this.__counter.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__isRefreshing.aboutToBeDeleted();
        this.__counter.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get isRefreshing() {
        return this.__isRefreshing.get();
    }
    set isRefreshing(newValue) {
        this.__isRefreshing.set(newValue);
    }
    get counter() {
        return this.__counter.get();
    }
    set counter(newValue) {
        this.__counter.set(newValue);
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("pages/Refresh/Test1.ets(12:5)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('Pull Down and isRefreshing: ' + this.isRefreshing);
            Text.debugLine("pages/Refresh/Test1.ets(13:7)");
            Text.fontSize(30);
            Text.margin(10);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Refresh.create({ refreshing: { value: this.isRefreshing, changeEvent: newValue => { this.isRefreshing = newValue; } }, offset: 120, friction: 100 });
            Refresh.debugLine("pages/Refresh/Test1.ets(17:7)");
            Refresh.onStateChange((refreshStatus) => {
                console.info('Refresh onStatueChange state is ' + refreshStatus);
            });
            if (!isInitialRender) {
                Refresh.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('Pull Down and refresh: ' + this.counter);
            Text.debugLine("pages/Refresh/Test1.ets(18:9)");
            Text.fontSize(30);
            Text.margin(10);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Refresh.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new RefreshExample(undefined, {}));
ViewStackProcessor.StopGetAccessRecording();
//# sourceMappingURL=Test1.js.map