// xxx.ets
import emitter from '@ohos:events.emitter';
/***
 * 开发者可以使用emit订阅某个事件并接收事件回调，可以减少开销，增强代码的可读性。
 */
let NextID = 0;
class ViewData {
    constructor(title, uri) {
        this.color = Color.Black;
        this.title = title;
        this.uri = uri;
        this.id = NextID++;
    }
}
class Gallery2 extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.dataList = [new ViewData('flower', { "id": 16777305, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" }), new ViewData('OMG', { "id": 16777305, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" }), new ViewData('OMG', { "id": 16777305, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" })];
        this.scroller = new Scroller();
        this.preIndex = -1;
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.dataList !== undefined) {
            this.dataList = params.dataList;
        }
        if (params.scroller !== undefined) {
            this.scroller = params.scroller;
        }
        if (params.preIndex !== undefined) {
            this.preIndex = params.preIndex;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("pages/AppStorage/Test4.ets(30:5)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Grid.create(this.scroller);
            Grid.debugLine("pages/AppStorage/Test4.ets(31:7)");
            Grid.columnsTemplate('1fr 1fr');
            if (!isInitialRender) {
                Grid.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            ForEach.create();
            const forEachItemGenFunction = _item => {
                const item = _item;
                {
                    const isLazyCreate = true && (Grid.willUseProxy() === true);
                    const itemCreation = (elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        GridItem.create(deepRenderFunction, isLazyCreate);
                        GridItem.aspectRatio(1);
                        GridItem.onClick(() => {
                            if (this.preIndex === item.id) {
                                return;
                            }
                            let innerEvent = { eventId: item.id };
                            // 选中态：黑变红
                            let eventData = {
                                data: {
                                    "colorTag": 1
                                }
                            };
                            emitter.emit(innerEvent, eventData);
                            if (this.preIndex != -1) {
                                console.info(`preIndex: ${this.preIndex}, index: ${item.id}, black`);
                                let innerEvent = { eventId: this.preIndex };
                                // 取消选中态：红变黑
                                let eventData = {
                                    data: {
                                        "colorTag": 0
                                    }
                                };
                                emitter.emit(innerEvent, eventData);
                            }
                            this.preIndex = item.id;
                        });
                        GridItem.debugLine("pages/AppStorage/Test4.ets(33:11)");
                        if (!isInitialRender) {
                            GridItem.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    };
                    const observedShallowRender = () => {
                        this.observeComponentCreation(itemCreation);
                        GridItem.pop();
                    };
                    const observedDeepRender = () => {
                        this.observeComponentCreation(itemCreation);
                        {
                            this.observeComponentCreation((elmtId, isInitialRender) => {
                                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                if (isInitialRender) {
                                    ViewPU.create(new TapImage(this, {
                                        uri: item.uri,
                                        index: item.id
                                    }, undefined, elmtId));
                                }
                                else {
                                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                                }
                                ViewStackProcessor.StopGetAccessRecording();
                            });
                        }
                        GridItem.pop();
                    };
                    const deepRenderFunction = (elmtId, isInitialRender) => {
                        itemCreation(elmtId, isInitialRender);
                        this.updateFuncByElmtId.set(elmtId, itemCreation);
                        {
                            this.observeComponentCreation((elmtId, isInitialRender) => {
                                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                if (isInitialRender) {
                                    ViewPU.create(new TapImage(this, {
                                        uri: item.uri,
                                        index: item.id
                                    }, undefined, elmtId));
                                }
                                else {
                                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                                }
                                ViewStackProcessor.StopGetAccessRecording();
                            });
                        }
                        GridItem.pop();
                    };
                    if (isLazyCreate) {
                        observedShallowRender();
                    }
                    else {
                        observedDeepRender();
                    }
                }
            };
            this.forEachUpdateFunction(elmtId, this.dataList, forEachItemGenFunction, (item) => JSON.stringify(item), false, false);
            if (!isInitialRender) {
                ForEach.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        ForEach.pop();
        Grid.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
export class TapImage extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__tapColor = new ObservedPropertySimplePU(Color.Black, this, "tapColor");
        this.index = 0;
        this.uri = {
            id: 0,
            type: 0,
            moduleName: "",
            bundleName: ""
        };
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.tapColor !== undefined) {
            this.tapColor = params.tapColor;
        }
        if (params.index !== undefined) {
            this.index = params.index;
        }
        if (params.uri !== undefined) {
            this.uri = params.uri;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__tapColor.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__tapColor.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get tapColor() {
        return this.__tapColor.get();
    }
    set tapColor(newValue) {
        this.__tapColor.set(newValue);
    }
    onTapIndexChange(colorTag) {
        if (colorTag.data != null) {
            this.tapColor = colorTag.data.colorTag ? Color.Red : Color.Black;
        }
    }
    aboutToAppear() {
        //定义事件ID
        let innerEvent = { eventId: this.index };
        emitter.on(innerEvent, data => {
            this.onTapIndexChange(data);
        });
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("pages/AppStorage/Test4.ets(98:5)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create(this.uri);
            Image.debugLine("pages/AppStorage/Test4.ets(99:7)");
            Image.objectFit(ImageFit.Cover);
            Image.border({ width: 5, style: BorderStyle.Dotted, color: this.tapColor });
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new Gallery2(undefined, {}));
ViewStackProcessor.StopGetAccessRecording();
//# sourceMappingURL=Test4.js.map