// xxx.ets
class ViewData {
    constructor(title, uri) {
        this.color = Color.Black;
        this.title = title;
        this.uri = uri;
    }
}
class Gallery2 extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.dataList = [new ViewData('flower', { "id": 16777305, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" }), new ViewData('OMG', { "id": 16777305, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" }), new ViewData('OMG', { "id": 16777305, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" })];
        this.scroller = new Scroller();
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.dataList !== undefined) {
            this.dataList = params.dataList;
        }
        if (params.scroller !== undefined) {
            this.scroller = params.scroller;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("pages/AppStorage/Test5.ets(20:5)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Grid.create(this.scroller);
            Grid.debugLine("pages/AppStorage/Test5.ets(21:7)");
            Grid.columnsTemplate('1fr 1fr');
            if (!isInitialRender) {
                Grid.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            ForEach.create();
            const forEachItemGenFunction = (_item, index) => {
                const item = _item;
                {
                    const isLazyCreate = true && (Grid.willUseProxy() === true);
                    const itemCreation = (elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        GridItem.create(deepRenderFunction, isLazyCreate);
                        GridItem.aspectRatio(1);
                        GridItem.debugLine("pages/AppStorage/Test5.ets(23:11)");
                        if (!isInitialRender) {
                            GridItem.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    };
                    const observedShallowRender = () => {
                        this.observeComponentCreation(itemCreation);
                        GridItem.pop();
                    };
                    const observedDeepRender = () => {
                        this.observeComponentCreation(itemCreation);
                        {
                            this.observeComponentCreation((elmtId, isInitialRender) => {
                                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                if (isInitialRender) {
                                    ViewPU.create(new TapImage(this, {
                                        uri: item.uri,
                                        index: index
                                    }, undefined, elmtId));
                                }
                                else {
                                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                                }
                                ViewStackProcessor.StopGetAccessRecording();
                            });
                        }
                        GridItem.pop();
                    };
                    const deepRenderFunction = (elmtId, isInitialRender) => {
                        itemCreation(elmtId, isInitialRender);
                        this.updateFuncByElmtId.set(elmtId, itemCreation);
                        {
                            this.observeComponentCreation((elmtId, isInitialRender) => {
                                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                                if (isInitialRender) {
                                    ViewPU.create(new TapImage(this, {
                                        uri: item.uri,
                                        index: index
                                    }, undefined, elmtId));
                                }
                                else {
                                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                                }
                                ViewStackProcessor.StopGetAccessRecording();
                            });
                        }
                        GridItem.pop();
                    };
                    if (isLazyCreate) {
                        observedShallowRender();
                    }
                    else {
                        observedDeepRender();
                    }
                }
            };
            this.forEachUpdateFunction(elmtId, this.dataList, forEachItemGenFunction, undefined, true, false);
            if (!isInitialRender) {
                ForEach.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        ForEach.pop();
        Grid.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
export class TapImage extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__tapIndex = this.createStorageLink('tapIndex', -1, "tapIndex");
        this.__tapColor = new ObservedPropertySimplePU(Color.Black, this, "tapColor");
        this.index = 0;
        this.uri = {
            id: 0,
            type: 0,
            moduleName: "",
            bundleName: ""
        };
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.tapColor !== undefined) {
            this.tapColor = params.tapColor;
        }
        if (params.index !== undefined) {
            this.index = params.index;
        }
        if (params.uri !== undefined) {
            this.uri = params.uri;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__tapColor.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__tapIndex.aboutToBeDeleted();
        this.__tapColor.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get tapIndex() {
        return this.__tapIndex.get();
    }
    set tapIndex(newValue) {
        this.__tapIndex.set(newValue);
    }
    get tapColor() {
        return this.__tapColor.get();
    }
    set tapColor(newValue) {
        this.__tapColor.set(newValue);
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("pages/AppStorage/Test5.ets(53:5)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Image.create(this.uri);
            Image.debugLine("pages/AppStorage/Test5.ets(54:7)");
            Image.objectFit(ImageFit.Cover);
            Image.onClick(() => {
                this.tapIndex = this.index;
                console.log('click index', this.index);
            });
            Image.border({
                width: 5,
                style: BorderStyle.Dotted,
                // color: (this.tapIndex >= 0 && this.index === this.tapIndex) ? Color.Red : Color.Black
                color: (this.index === this.tapIndex) ? Color.Red : Color.Black
            });
            if (!isInitialRender) {
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new Gallery2(undefined, {}));
ViewStackProcessor.StopGetAccessRecording();
//# sourceMappingURL=Test5.js.map