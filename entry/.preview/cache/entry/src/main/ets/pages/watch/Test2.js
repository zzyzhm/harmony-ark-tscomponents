"use strict";
/****
 * 处理步骤如下：
 BasketModifier组件的Button.onClick向BasketModifier shopBasket中添加条目；
 @Link装饰的BasketViewer shopBasket值发生变化；
 状态管理框架调用@Watch函数BasketViewer onBasketUpdated 更新BasketViewer TotalPurchase的值；
 @Link shopBasket的改变，新增了数组项，ForEach组件会执行item Builder，
 渲染构建新的Item项；@State totalPurchase改变，对应的Text组件也重新渲染；重新渲染是异步发生的。
 */
class PurchaseItem {
    constructor(price) {
        this.id = PurchaseItem.NextId++;
        this.price = price;
    }
}
PurchaseItem.NextId = 0;
class BasketViewer extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__shopBasket = new SynchedPropertyObjectTwoWayPU(params.shopBasket, this, "shopBasket");
        this.__totalPurchase = new ObservedPropertySimplePU(0, this, "totalPurchase");
        this.setInitiallyProvidedValue(params);
        this.declareWatch("shopBasket", this.onBasketUpdated);
    }
    setInitiallyProvidedValue(params) {
        if (params.totalPurchase !== undefined) {
            this.totalPurchase = params.totalPurchase;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__shopBasket.purgeDependencyOnElmtId(rmElmtId);
        this.__totalPurchase.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__shopBasket.aboutToBeDeleted();
        this.__totalPurchase.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get shopBasket() {
        return this.__shopBasket.get();
    }
    set shopBasket(newValue) {
        this.__shopBasket.set(newValue);
    }
    get totalPurchase() {
        return this.__totalPurchase.get();
    }
    set totalPurchase(newValue) {
        this.__totalPurchase.set(newValue);
    }
    updateTotal() {
        let total = this.shopBasket.reduce((sum, i) => sum + i.price, 0);
        // 超过100欧元可享受折扣
        if (total >= 100) {
            total = 0.9 * total;
        }
        return total;
    }
    // @Watch 回调
    onBasketUpdated(propName) {
        this.totalPurchase = this.updateTotal();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("pages/watch/Test2.ets(40:5)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            ForEach.create();
            const forEachItemGenFunction = _item => {
                const item = _item;
                this.observeComponentCreation((elmtId, isInitialRender) => {
                    ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                    Text.create(`Price: ${item.price.toFixed(2)} €`);
                    Text.debugLine("pages/watch/Test2.ets(43:11)");
                    if (!isInitialRender) {
                        Text.pop();
                    }
                    ViewStackProcessor.StopGetAccessRecording();
                });
                Text.pop();
            };
            this.forEachUpdateFunction(elmtId, this.shopBasket, forEachItemGenFunction, item => item.id.toString(), false, false);
            if (!isInitialRender) {
                ForEach.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        ForEach.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(`Total: ${this.totalPurchase.toFixed(2)} €`);
            Text.debugLine("pages/watch/Test2.ets(47:7)");
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
class BasketModifier extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__shopBasket = new ObservedPropertyObjectPU([], this, "shopBasket");
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.shopBasket !== undefined) {
            this.shopBasket = params.shopBasket;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__shopBasket.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__shopBasket.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get shopBasket() {
        return this.__shopBasket.get();
    }
    set shopBasket(newValue) {
        this.__shopBasket.set(newValue);
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("pages/watch/Test2.ets(58:5)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Button.createWithLabel('Add to basket');
            Button.debugLine("pages/watch/Test2.ets(59:7)");
            Button.onClick(() => {
                this.shopBasket.push(new PurchaseItem(Math.round(100 * Math.random())));
            });
            if (!isInitialRender) {
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Button.pop();
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new BasketViewer(this, { shopBasket: this.__shopBasket }, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new BasketModifier(undefined, {}));
ViewStackProcessor.StopGetAccessRecording();
//# sourceMappingURL=Test2.js.map