"use strict";
class CountDownComponent extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__count = new SynchedPropertySimpleOneWayPU(params.count, this, "count");
        this.costOfOneAttempt = 1;
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.costOfOneAttempt !== undefined) {
            this.costOfOneAttempt = params.costOfOneAttempt;
        }
    }
    updateStateVars(params) {
        this.__count.reset(params.count);
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__count.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__count.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get count() {
        return this.__count.get();
    }
    set count(newValue) {
        this.__count.set(newValue);
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("pages/prop/PropTest1.ets(7:5)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            If.create();
            if (this.count > 0) {
                this.ifElseBranchUpdateFunction(0, () => {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Text.create(`You have ${this.count} Nuggets left`);
                        Text.debugLine("pages/prop/PropTest1.ets(9:9)");
                        if (!isInitialRender) {
                            Text.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    Text.pop();
                });
            }
            else {
                this.ifElseBranchUpdateFunction(1, () => {
                    this.observeComponentCreation((elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        Text.create('Game over!');
                        Text.debugLine("pages/prop/PropTest1.ets(11:9)");
                        if (!isInitialRender) {
                            Text.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    });
                    Text.pop();
                });
            }
            if (!isInitialRender) {
                If.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        If.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // @Prop装饰的变量不会同步给父组件
            Button.createWithLabel(`Try again`);
            Button.debugLine("pages/prop/PropTest1.ets(14:7)");
            // @Prop装饰的变量不会同步给父组件
            Button.onClick(() => {
                //console.log('costOfOneAttempt', this.costOfOneAttempt)
                this.count -= this.costOfOneAttempt;
            });
            if (!isInitialRender) {
                // @Prop装饰的变量不会同步给父组件
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        // @Prop装饰的变量不会同步给父组件
        Button.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
class ParentComponent extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__countDownStartValue = new ObservedPropertySimplePU(10, this, "countDownStartValue");
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.countDownStartValue !== undefined) {
            this.countDownStartValue = params.countDownStartValue;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__countDownStartValue.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__countDownStartValue.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get countDownStartValue() {
        return this.__countDownStartValue.get();
    }
    set countDownStartValue(newValue) {
        this.__countDownStartValue.set(newValue);
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("pages/prop/PropTest1.ets(28:5)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create(`Grant ${this.countDownStartValue} nuggets to play.`);
            Text.debugLine("pages/prop/PropTest1.ets(29:7)");
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // 父组件的数据源的修改会同步给子组件
            Button.createWithLabel(`+1 - Nuggets in New Game`);
            Button.debugLine("pages/prop/PropTest1.ets(31:7)");
            // 父组件的数据源的修改会同步给子组件
            Button.onClick(() => {
                this.countDownStartValue += 1;
            });
            if (!isInitialRender) {
                // 父组件的数据源的修改会同步给子组件
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        // 父组件的数据源的修改会同步给子组件
        Button.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // 父组件的修改会同步给子组件
            Button.createWithLabel(`-1  - Nuggets in New Game`);
            Button.debugLine("pages/prop/PropTest1.ets(35:7)");
            // 父组件的修改会同步给子组件
            Button.onClick(() => {
                this.countDownStartValue -= 1;
            });
            if (!isInitialRender) {
                // 父组件的修改会同步给子组件
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        // 父组件的修改会同步给子组件
        Button.pop();
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new CountDownComponent(this, { count: this.countDownStartValue, costOfOneAttempt: 2 }, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {
                        count: this.countDownStartValue
                    });
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new ParentComponent(undefined, {}));
ViewStackProcessor.StopGetAccessRecording();
//# sourceMappingURL=PropTest1.js.map