import { ClassA, ClassB } from '@bundle:com.example.component/entry/ets/pages/observedAndObjectLink/Test1';
class ViewA extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.label = 'ViewA1';
        this.__classA = new SynchedPropertyNesedObjectPU(params.classA, this, "classA");
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.label !== undefined) {
            this.label = params.label;
        }
        this.__classA.set(params.classA);
    }
    updateStateVars(params) {
        this.__classA.set(params.classA);
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__classA.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__classA.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get classA() {
        return this.__classA.get();
    }
    //public aa:ClassA  = new ClassA(11);
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("pages/observedAndObjectLink/Test2.ets(10:5)");
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Button.createWithLabel(`ViewA [${this.label}] this.a.c=${this.classA.num} +1`);
            Button.debugLine("pages/observedAndObjectLink/Test2.ets(11:7)");
            Button.onClick(() => {
                this.classA.num += 1;
            });
            if (!isInitialRender) {
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Button.pop();
        Row.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
class ViewB extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__b = new ObservedPropertyObjectPU(new ClassB(new ClassA(0)), this, "b");
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.b !== undefined) {
            this.b = params.b;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__b.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__b.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get b() {
        return this.__b.get();
    }
    set b(newValue) {
        this.__b.set(newValue);
    }
    //不能在 @Entry 中使用
    //@ObjectLink a: ClassA;
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("pages/observedAndObjectLink/Test2.ets(27:5)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new ViewA(this, { label: 'ViewA #1', classA: this.b.a_class }, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {
                        classA: this.b.a_class
                    });
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new ViewA(this, { label: 'ViewA #2', classA: this.b.a_class }, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {
                        classA: this.b.a_class
                    });
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Button.createWithLabel(`ViewB: this.b.a.c+= 1`);
            Button.debugLine("pages/observedAndObjectLink/Test2.ets(31:7)");
            Button.onClick(() => {
                this.b.a_class.num += 1;
            });
            if (!isInitialRender) {
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Button.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Button.createWithLabel(`ViewB: this.b.a = new ClassA(0)`);
            Button.debugLine("pages/observedAndObjectLink/Test2.ets(35:7)");
            Button.onClick(() => {
                this.b.a_class = new ClassA(0);
            });
            if (!isInitialRender) {
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Button.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Button.createWithLabel(`ViewB: this.b = new ClassB(ClassA(0))`);
            Button.debugLine("pages/observedAndObjectLink/Test2.ets(39:7)");
            Button.onClick(() => {
                this.b = new ClassB(new ClassA(0));
            });
            if (!isInitialRender) {
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Button.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new ViewB(undefined, {}));
ViewStackProcessor.StopGetAccessRecording();
//# sourceMappingURL=Test2.js.map