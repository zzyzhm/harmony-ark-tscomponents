"use strict";
// 创建新实例并使用给定对象初始化
let storage = new LocalStorage({ 'PropA': 47 });
class Child extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        // @LocalStorageLink变量装饰器与LocalStorage中的'PropA'属性建立双向绑定
        this.__storLink2 = this.createLocalStorageLink("PropA", 1, "storLink2");
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        this.__storLink2.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get storLink2() {
        return this.__storLink2.get();
    }
    set storLink2(newValue) {
        this.__storLink2.set(newValue);
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Button.createWithLabel(`Child from LocalStorage ${this.storLink2}`);
            Button.debugLine("pages/localStorage/Test1.ets(10:5)");
            Button.onClick(() => this.storLink2 += 1);
            if (!isInitialRender) {
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Button.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
class CompA extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        // @LocalStorageLink变量装饰器与LocalStorage中的'PropA'属性建立双向绑定
        this.__storLink1 = this.createLocalStorageLink("PropA", 1, "storLink1");
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        this.__storLink1.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get storLink1() {
        return this.__storLink1.get();
    }
    set storLink1(newValue) {
        this.__storLink1.set(newValue);
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create({ space: 15 });
            Column.debugLine("pages/localStorage/Test1.ets(23:5)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            //// initial value from LocalStorage will be 47, because 'PropA' initialized already
            Button.createWithLabel(`Parent from LocalStorage ${this.storLink1}`);
            Button.debugLine("pages/localStorage/Test1.ets(26:7)");
            //// initial value from LocalStorage will be 47, because 'PropA' initialized already
            Button.onClick(() => this.storLink1 += 1);
            if (!isInitialRender) {
                //// initial value from LocalStorage will be 47, because 'PropA' initialized already
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        //// initial value from LocalStorage will be 47, because 'PropA' initialized already
        Button.pop();
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new 
                    // @Component子组件自动获得对CompA LocalStorage实例的访问权限。
                    Child(this, {}, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new CompA(undefined, {}, storage));
ViewStackProcessor.StopGetAccessRecording();
//# sourceMappingURL=Test1.js.map