"use strict";
// 创建新实例并使用给定对象初始化
let storage = new LocalStorage({ 'PropA': 47 });
class CompA extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        // @LocalStorageProp变量装饰器与LocalStorage中的'PropA'属性建立单向绑定
        this.__storProp1 = this.createLocalStorageProp("PropA", 1, "storProp1");
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        this.__storProp1.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get storProp1() {
        return this.__storProp1.get();
    }
    set storProp1(newValue) {
        this.__storProp1.set(newValue);
    }
    //下面是双向绑定
    //@LocalStorageLink('PropA') storProp1: number = 1;
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create({ space: 15 });
            Column.debugLine("pages/localStorage/Test2.ets(15:5)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // 点击后从47开始加1，只改变当前组件显示的storProp1，不会同步到LocalStorage中
            Button.createWithLabel(`Parent from LocalStorage ${this.storProp1}`);
            Button.debugLine("pages/localStorage/Test2.ets(17:7)");
            // 点击后从47开始加1，只改变当前组件显示的storProp1，不会同步到LocalStorage中
            Button.onClick(() => this.storProp1 += 1);
            if (!isInitialRender) {
                // 点击后从47开始加1，只改变当前组件显示的storProp1，不会同步到LocalStorage中
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        // 点击后从47开始加1，只改变当前组件显示的storProp1，不会同步到LocalStorage中
        Button.pop();
        {
            this.observeComponentCreation((elmtId, isInitialRender) => {
                ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                if (isInitialRender) {
                    ViewPU.create(new Child(this, {}, undefined, elmtId));
                }
                else {
                    this.updateStateVarsOfChildByElmtId(elmtId, {});
                }
                ViewStackProcessor.StopGetAccessRecording();
            });
        }
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
class Child extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        // @LocalStorageProp变量装饰器与LocalStorage中的'PropA'属性建立单向绑定
        this.__storProp2 = this.createLocalStorageLink("PropA", 2, "storProp2");
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        this.__storProp2.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get storProp2() {
        return this.__storProp2.get();
    }
    set storProp2(newValue) {
        this.__storProp2.set(newValue);
    }
    //@LocalStorageProp('PropA') storProp2: number = 2;
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create({ space: 15 });
            Column.debugLine("pages/localStorage/Test2.ets(31:5)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // 当CompA改变时，当前storProp2不会改变，显示47
            Text.create(`Parent from LocalStorage ${this.storProp2}`);
            Text.debugLine("pages/localStorage/Test2.ets(33:7)");
            if (!isInitialRender) {
                // 当CompA改变时，当前storProp2不会改变，显示47
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        // 当CompA改变时，当前storProp2不会改变，显示47
        Text.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new CompA(undefined, {}, storage));
ViewStackProcessor.StopGetAccessRecording();
//# sourceMappingURL=Test2.js.map