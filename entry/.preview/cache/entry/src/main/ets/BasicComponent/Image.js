"use strict";
class ImageExample1 extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("BasicComponent/Image.ets(28:5)");
            Column.height(320);
            Column.width(360);
            Column.padding({ right: 10, top: 10 });
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Flex.create({ direction: FlexDirection.Column, alignItems: ItemAlign.Start });
            Flex.debugLine("BasicComponent/Image.ets(29:7)");
            if (!isInitialRender) {
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("BasicComponent/Image.ets(30:9)");
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // 加载png格式图片
            Image.create({ "id": 16777220, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" });
            Image.debugLine("BasicComponent/Image.ets(32:11)");
            // 加载png格式图片
            Image.width(110);
            // 加载png格式图片
            Image.height(110);
            // 加载png格式图片
            Image.margin(15);
            // 加载png格式图片
            Image.overlay('png', { align: Alignment.Bottom, offset: { x: 0, y: 20 } });
            if (!isInitialRender) {
                // 加载png格式图片
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // 加载gif格式图片
            Image.create({ "id": 16777317, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" });
            Image.debugLine("BasicComponent/Image.ets(36:11)");
            // 加载gif格式图片
            Image.width(110);
            // 加载gif格式图片
            Image.height(110);
            // 加载gif格式图片
            Image.margin(15);
            // 加载gif格式图片
            Image.overlay('gif', { align: Alignment.Bottom, offset: { x: 0, y: 20 } });
            if (!isInitialRender) {
                // 加载gif格式图片
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Row.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("BasicComponent/Image.ets(40:9)");
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // 加载svg格式图片
            Image.create({ "id": 16777328, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" });
            Image.debugLine("BasicComponent/Image.ets(42:11)");
            // 加载svg格式图片
            Image.width(110);
            // 加载svg格式图片
            Image.height(110);
            // 加载svg格式图片
            Image.margin(15);
            // 加载svg格式图片
            Image.overlay('svg', { align: Alignment.Bottom, offset: { x: 0, y: 20 } });
            // 加载svg格式图片
            Image.onFinish(() => {
                console.log('finish.....................');
            });
            if (!isInitialRender) {
                // 加载svg格式图片
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // 加载jpg格式图片
            Image.create({ "id": 16777318, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" });
            Image.debugLine("BasicComponent/Image.ets(49:11)");
            // 加载jpg格式图片
            Image.width(110);
            // 加载jpg格式图片
            Image.height(110);
            // 加载jpg格式图片
            Image.margin(15);
            // 加载jpg格式图片
            Image.overlay('jpg', { align: Alignment.Bottom, offset: { x: 0, y: 20 } });
            if (!isInitialRender) {
                // 加载jpg格式图片
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Row.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("BasicComponent/Image.ets(54:9)");
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // 直接加载网络地址，请填写一个具体的网络图片地址 百度图片
            Image.create("https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png");
            Image.debugLine("BasicComponent/Image.ets(56:11)");
            // 直接加载网络地址，请填写一个具体的网络图片地址 百度图片
            Image.alt({ "id": 16777306, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" });
            // 直接加载网络地址，请填写一个具体的网络图片地址 百度图片
            Image.width(200);
            // 直接加载网络地址，请填写一个具体的网络图片地址 百度图片
            Image.height(100);
            // 直接加载网络地址，请填写一个具体的网络图片地址 百度图片
            Image.onComplete((msg) => {
                console.log('windth:', msg.width, 'height:', msg.height);
            });
            // 直接加载网络地址，请填写一个具体的网络图片地址 百度图片
            Image.onError(() => {
                console.log('load image fail');
            });
            if (!isInitialRender) {
                // 直接加载网络地址，请填写一个具体的网络图片地址 百度图片
                Image.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Row.pop();
        Flex.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new ImageExample1(undefined, {}));
ViewStackProcessor.StopGetAccessRecording();
//# sourceMappingURL=Image.js.map