"use strict";
class DataPanelExample extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.valueArr = [20, 10, 10, 10, 10, 10, 10, 10, 10];
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.valueArr !== undefined) {
            this.valueArr = params.valueArr;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create({ space: 5 });
            Column.debugLine("BasicComponent/DataPanel.ets(18:5)");
            Column.width('100%');
            Column.margin({ top: 5 });
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("BasicComponent/DataPanel.ets(19:7)");
            Row.margin({ bottom: 59 });
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Stack.create();
            Stack.debugLine("BasicComponent/DataPanel.ets(20:9)");
            Stack.margin({ right: 44 });
            if (!isInitialRender) {
                Stack.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            DataPanel.create({ values: [25, 15, 10], max: 100, type: DataPanelType.Circle });
            DataPanel.debugLine("BasicComponent/DataPanel.ets(21:11)");
            DataPanel.width(168);
            DataPanel.height(168);
            if (!isInitialRender) {
                DataPanel.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        DataPanel.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("BasicComponent/DataPanel.ets(22:11)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('50');
            Text.debugLine("BasicComponent/DataPanel.ets(23:13)");
            Text.fontSize(35);
            Text.fontColor('#182431');
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('1.0.0');
            Text.debugLine("BasicComponent/DataPanel.ets(24:13)");
            Text.fontSize(9.33);
            Text.lineHeight(12.83);
            Text.fontWeight(500);
            Text.opacity(0.6);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Column.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('%');
            Text.debugLine("BasicComponent/DataPanel.ets(27:11)");
            Text.fontSize(9.33);
            Text.lineHeight(12.83);
            Text.fontWeight(500);
            Text.opacity(0.6);
            Text.position({ x: 104.42, y: 78.17 });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Stack.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Stack.create();
            Stack.debugLine("BasicComponent/DataPanel.ets(35:9)");
            if (!isInitialRender) {
                Stack.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            DataPanel.create({ values: [50, 12, 8, 5], max: 100, type: DataPanelType.Circle });
            DataPanel.debugLine("BasicComponent/DataPanel.ets(36:11)");
            DataPanel.width(168);
            DataPanel.height(168);
            if (!isInitialRender) {
                DataPanel.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        DataPanel.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("BasicComponent/DataPanel.ets(37:11)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('75');
            Text.debugLine("BasicComponent/DataPanel.ets(38:13)");
            Text.fontSize(35);
            Text.fontColor('#182431');
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('已使用98GB/128GB');
            Text.debugLine("BasicComponent/DataPanel.ets(39:13)");
            Text.fontSize(8.17);
            Text.lineHeight(11.08);
            Text.fontWeight(500);
            Text.opacity(0.6);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Column.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('%');
            Text.debugLine("BasicComponent/DataPanel.ets(42:11)");
            Text.fontSize(9.33);
            Text.lineHeight(12.83);
            Text.fontWeight(500);
            Text.opacity(0.6);
            Text.position({ x: 104.42, y: 78.17 });
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Stack.pop();
        Row.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            DataPanel.create({ values: this.valueArr, max: 100, type: DataPanelType.Line });
            DataPanel.debugLine("BasicComponent/DataPanel.ets(51:7)");
            DataPanel.width(300);
            DataPanel.height(10);
            if (!isInitialRender) {
                DataPanel.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        DataPanel.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new DataPanelExample(undefined, {}));
ViewStackProcessor.StopGetAccessRecording();
//# sourceMappingURL=DataPanel.js.map