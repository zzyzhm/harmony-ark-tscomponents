"use strict";
class BlankExample extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("BasicComponent/Blank.ets(13:5)");
            Column.backgroundColor(0xEFEFEF);
            Column.padding(20);
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Blank.create(10);
            Blank.debugLine("BasicComponent/Blank.ets(14:7)");
            Blank.color(Color.Red);
            if (!isInitialRender) {
                Blank.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Blank.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("BasicComponent/Blank.ets(16:7)");
            Row.width('100%');
            Row.backgroundColor(0xFFFFFF);
            Row.borderRadius(15);
            Row.padding({ left: 12 });
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('Bluetooth');
            Text.debugLine("BasicComponent/Blank.ets(17:9)");
            Text.fontSize(18);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Blank.create();
            Blank.debugLine("BasicComponent/Blank.ets(18:9)");
            if (!isInitialRender) {
                Blank.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Blank.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Toggle.create({ type: ToggleType.Switch });
            Toggle.debugLine("BasicComponent/Blank.ets(19:9)");
            Toggle.margin({ top: 14, bottom: 14, left: 6, right: 6 });
            if (!isInitialRender) {
                Toggle.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Toggle.pop();
        Row.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Blank.create('50');
            Blank.debugLine("BasicComponent/Blank.ets(22:7)");
            if (!isInitialRender) {
                Blank.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Blank.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // blank父组件不设置宽度时，Blank失效，可以通过设置min最小宽度填充固定宽度
            Row.create();
            Row.debugLine("BasicComponent/Blank.ets(25:7)");
            // blank父组件不设置宽度时，Blank失效，可以通过设置min最小宽度填充固定宽度
            Row.backgroundColor(0xFFFFFF);
            // blank父组件不设置宽度时，Blank失效，可以通过设置min最小宽度填充固定宽度
            Row.borderRadius(15);
            // blank父组件不设置宽度时，Blank失效，可以通过设置min最小宽度填充固定宽度
            Row.padding({ left: 12 });
            if (!isInitialRender) {
                // blank父组件不设置宽度时，Blank失效，可以通过设置min最小宽度填充固定宽度
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('Bluetooth');
            Text.debugLine("BasicComponent/Blank.ets(26:9)");
            Text.fontSize(18);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Blank.create(5);
            Blank.debugLine("BasicComponent/Blank.ets(27:9)");
            Blank.color(Color.Yellow);
            if (!isInitialRender) {
                Blank.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Blank.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Toggle.create({ type: ToggleType.Switch });
            Toggle.debugLine("BasicComponent/Blank.ets(28:9)");
            Toggle.margin({ top: 14, bottom: 14, left: 6, right: 6 });
            if (!isInitialRender) {
                Toggle.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Toggle.pop();
        // blank父组件不设置宽度时，Blank失效，可以通过设置min最小宽度填充固定宽度
        Row.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Blank.create('80');
            Blank.debugLine("BasicComponent/Blank.ets(31:7)");
            if (!isInitialRender) {
                Blank.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Blank.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("BasicComponent/Blank.ets(33:7)");
            Row.backgroundColor(0xFFFFFF);
            Row.borderRadius(15);
            Row.padding({ left: 12 });
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('Bluetooth');
            Text.debugLine("BasicComponent/Blank.ets(34:9)");
            Text.fontSize(18);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // 设置最小宽度为160
            Blank.create('160');
            Blank.debugLine("BasicComponent/Blank.ets(36:9)");
            // 设置最小宽度为160
            Blank.color(Color.Yellow);
            if (!isInitialRender) {
                // 设置最小宽度为160
                Blank.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        // 设置最小宽度为160
        Blank.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Toggle.create({ type: ToggleType.Switch });
            Toggle.debugLine("BasicComponent/Blank.ets(37:9)");
            Toggle.margin({ top: 14, bottom: 14, left: 6, right: 6 });
            if (!isInitialRender) {
                Toggle.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Toggle.pop();
        Row.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new BlankExample(undefined, {}));
ViewStackProcessor.StopGetAccessRecording();
//# sourceMappingURL=Blank.js.map