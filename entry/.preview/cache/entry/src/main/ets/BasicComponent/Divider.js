"use strict";
class DividerExample extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("BasicComponent/Divider.ets(11:5)");
            Column.width('100%');
            Column.padding({ top: 24 });
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // 使用横向分割线场景
            Text.create('Horizontal divider');
            Text.debugLine("BasicComponent/Divider.ets(13:7)");
            // 使用横向分割线场景
            Text.fontSize(22);
            // 使用横向分割线场景
            Text.fontColor(0xCCCCCC);
            if (!isInitialRender) {
                // 使用横向分割线场景
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        // 使用横向分割线场景
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Divider.create();
            Divider.debugLine("BasicComponent/Divider.ets(14:7)");
            Divider.strokeWidth('8vp');
            Divider.color('#ff1f84ea');
            if (!isInitialRender) {
                Divider.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            List.create();
            List.debugLine("BasicComponent/Divider.ets(15:7)");
            List.padding({ left: 24, bottom: 8 });
            if (!isInitialRender) {
                List.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            ForEach.create();
            const forEachItemGenFunction = _item => {
                const item = _item;
                {
                    const isLazyCreate = true;
                    const itemCreation = (elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        ListItem.create(deepRenderFunction, isLazyCreate);
                        ListItem.width(244);
                        ListItem.height(48);
                        ListItem.debugLine("BasicComponent/Divider.ets(17:11)");
                        if (!isInitialRender) {
                            ListItem.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    };
                    const observedShallowRender = () => {
                        this.observeComponentCreation(itemCreation);
                        ListItem.pop();
                    };
                    const observedDeepRender = () => {
                        this.observeComponentCreation(itemCreation);
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            Text.create('list' + item);
                            Text.debugLine("BasicComponent/Divider.ets(18:13)");
                            Text.width('100%');
                            Text.fontSize(14);
                            Text.fontColor('#182431');
                            Text.textAlign(TextAlign.Start);
                            if (!isInitialRender) {
                                Text.pop();
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                        Text.pop();
                        ListItem.pop();
                    };
                    const deepRenderFunction = (elmtId, isInitialRender) => {
                        itemCreation(elmtId, isInitialRender);
                        this.updateFuncByElmtId.set(elmtId, itemCreation);
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            Text.create('list' + item);
                            Text.debugLine("BasicComponent/Divider.ets(18:13)");
                            Text.width('100%');
                            Text.fontSize(14);
                            Text.fontColor('#182431');
                            Text.textAlign(TextAlign.Start);
                            if (!isInitialRender) {
                                Text.pop();
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                        Text.pop();
                        ListItem.pop();
                    };
                    if (isLazyCreate) {
                        observedShallowRender();
                    }
                    else {
                        observedDeepRender();
                    }
                }
            };
            this.forEachUpdateFunction(elmtId, [1, 2, 3], forEachItemGenFunction, item => item.toString(), false, false);
            if (!isInitialRender) {
                ForEach.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        ForEach.pop();
        List.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            //分隔符  分割线宽度。 默认值：1 单位：vp
            // strokeWidth:分割线宽度 默认1
            Divider.create();
            Divider.debugLine("BasicComponent/Divider.ets(25:7)");
            //分隔符  分割线宽度。 默认值：1 单位：vp
            // strokeWidth:分割线宽度 默认1
            Divider.strokeWidth('8vp');
            //分隔符  分割线宽度。 默认值：1 单位：vp
            // strokeWidth:分割线宽度 默认1
            Divider.color('#fff3f328');
            if (!isInitialRender) {
                //分隔符  分割线宽度。 默认值：1 单位：vp
                // strokeWidth:分割线宽度 默认1
                Divider.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            List.create();
            List.debugLine("BasicComponent/Divider.ets(26:7)");
            List.padding({ left: 24, top: 8 });
            if (!isInitialRender) {
                List.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            ForEach.create();
            const forEachItemGenFunction = _item => {
                const item = _item;
                {
                    const isLazyCreate = true;
                    const itemCreation = (elmtId, isInitialRender) => {
                        ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                        ListItem.create(deepRenderFunction, isLazyCreate);
                        ListItem.width(244);
                        ListItem.height(48);
                        ListItem.debugLine("BasicComponent/Divider.ets(28:11)");
                        if (!isInitialRender) {
                            ListItem.pop();
                        }
                        ViewStackProcessor.StopGetAccessRecording();
                    };
                    const observedShallowRender = () => {
                        this.observeComponentCreation(itemCreation);
                        ListItem.pop();
                    };
                    const observedDeepRender = () => {
                        this.observeComponentCreation(itemCreation);
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            Text.create('list' + item);
                            Text.debugLine("BasicComponent/Divider.ets(30:13)");
                            Text.width('100%');
                            Text.fontSize(14);
                            Text.fontColor('#182431');
                            Text.textAlign(TextAlign.Start);
                            if (!isInitialRender) {
                                Text.pop();
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                        Text.pop();
                        ListItem.pop();
                    };
                    const deepRenderFunction = (elmtId, isInitialRender) => {
                        itemCreation(elmtId, isInitialRender);
                        this.updateFuncByElmtId.set(elmtId, itemCreation);
                        this.observeComponentCreation((elmtId, isInitialRender) => {
                            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
                            Text.create('list' + item);
                            Text.debugLine("BasicComponent/Divider.ets(30:13)");
                            Text.width('100%');
                            Text.fontSize(14);
                            Text.fontColor('#182431');
                            Text.textAlign(TextAlign.Start);
                            if (!isInitialRender) {
                                Text.pop();
                            }
                            ViewStackProcessor.StopGetAccessRecording();
                        });
                        Text.pop();
                        ListItem.pop();
                    };
                    if (isLazyCreate) {
                        observedShallowRender();
                    }
                    else {
                        observedDeepRender();
                    }
                }
            };
            this.forEachUpdateFunction(elmtId, [4, 5], forEachItemGenFunction, item => item.toString(), false, false);
            if (!isInitialRender) {
                ForEach.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        ForEach.pop();
        List.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // 使用纵向分割线场景
            Text.create('Vertical divider');
            Text.debugLine("BasicComponent/Divider.ets(36:7)");
            // 使用纵向分割线场景
            Text.fontSize(22);
            // 使用纵向分割线场景
            Text.fontColor(0xCCCCCC);
            if (!isInitialRender) {
                // 使用纵向分割线场景
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        // 使用纵向分割线场景
        Text.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("BasicComponent/Divider.ets(37:7)");
            Column.width('100%');
            Column.height(168);
            Column.backgroundColor('#F1F3F5');
            Column.justifyContent(FlexAlign.Center);
            Column.margin({ top: 8 });
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("BasicComponent/Divider.ets(38:9)");
            Column.width(336);
            Column.height(152);
            Column.backgroundColor('#FFFFFF');
            Column.borderRadius(24);
            Column.padding(24);
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("BasicComponent/Divider.ets(39:11)");
            Row.width(288);
            Row.height(64);
            Row.backgroundColor('#30C9F0');
            Row.opacity(0.3);
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Row.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("BasicComponent/Divider.ets(40:11)");
            Row.margin({ top: 17 });
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Button.createWithLabel('Button');
            Button.debugLine("BasicComponent/Divider.ets(41:13)");
            Button.width(136);
            Button.height(22);
            Button.fontSize(16);
            Button.fontColor('#007DFF');
            Button.fontWeight(500);
            Button.backgroundColor(Color.Transparent);
            if (!isInitialRender) {
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Button.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            /**
             * vertical:使用水平分割线还是垂直分割线。false：水平分割线；true：垂直分割线。默认值：false
             * color:分割线颜色 默认值：'#33182431'
             * strokeWidth:分割线宽度 默认1
             * lineCap:分割线的端点样式。   默认值：LineCapStyle.Butt
             */
            Divider.create();
            Divider.debugLine("BasicComponent/Divider.ets(55:13)");
            /**
             * vertical:使用水平分割线还是垂直分割线。false：水平分割线；true：垂直分割线。默认值：false
             * color:分割线颜色 默认值：'#33182431'
             * strokeWidth:分割线宽度 默认1
             * lineCap:分割线的端点样式。   默认值：LineCapStyle.Butt
             */
            Divider.vertical(true);
            /**
             * vertical:使用水平分割线还是垂直分割线。false：水平分割线；true：垂直分割线。默认值：false
             * color:分割线颜色 默认值：'#33182431'
             * strokeWidth:分割线宽度 默认1
             * lineCap:分割线的端点样式。   默认值：LineCapStyle.Butt
             */
            Divider.height(22);
            /**
             * vertical:使用水平分割线还是垂直分割线。false：水平分割线；true：垂直分割线。默认值：false
             * color:分割线颜色 默认值：'#33182431'
             * strokeWidth:分割线宽度 默认1
             * lineCap:分割线的端点样式。   默认值：LineCapStyle.Butt
             */
            Divider.color('#ffe5370b');
            /**
             * vertical:使用水平分割线还是垂直分割线。false：水平分割线；true：垂直分割线。默认值：false
             * color:分割线颜色 默认值：'#33182431'
             * strokeWidth:分割线宽度 默认1
             * lineCap:分割线的端点样式。   默认值：LineCapStyle.Butt
             */
            Divider.opacity(0.6);
            /**
             * vertical:使用水平分割线还是垂直分割线。false：水平分割线；true：垂直分割线。默认值：false
             * color:分割线颜色 默认值：'#33182431'
             * strokeWidth:分割线宽度 默认1
             * lineCap:分割线的端点样式。   默认值：LineCapStyle.Butt
             */
            Divider.margin({ left: 8, right: 8 });
            /**
             * vertical:使用水平分割线还是垂直分割线。false：水平分割线；true：垂直分割线。默认值：false
             * color:分割线颜色 默认值：'#33182431'
             * strokeWidth:分割线宽度 默认1
             * lineCap:分割线的端点样式。   默认值：LineCapStyle.Butt
             */
            Divider.strokeWidth(4);
            /**
             * vertical:使用水平分割线还是垂直分割线。false：水平分割线；true：垂直分割线。默认值：false
             * color:分割线颜色 默认值：'#33182431'
             * strokeWidth:分割线宽度 默认1
             * lineCap:分割线的端点样式。   默认值：LineCapStyle.Butt
             */
            Divider.lineCap(LineCapStyle.Butt);
            if (!isInitialRender) {
                /**
                 * vertical:使用水平分割线还是垂直分割线。false：水平分割线；true：垂直分割线。默认值：false
                 * color:分割线颜色 默认值：'#33182431'
                 * strokeWidth:分割线宽度 默认1
                 * lineCap:分割线的端点样式。   默认值：LineCapStyle.Butt
                 */
                Divider.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Button.createWithLabel('Button');
            Button.debugLine("BasicComponent/Divider.ets(59:13)");
            Button.width(136);
            Button.height(22);
            Button.fontSize(16);
            Button.fontColor('#007DFF');
            Button.fontWeight(500);
            Button.backgroundColor(Color.Transparent);
            if (!isInitialRender) {
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Button.pop();
        Row.pop();
        Column.pop();
        Column.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new DividerExample(undefined, {}));
ViewStackProcessor.StopGetAccessRecording();
//# sourceMappingURL=Divider.js.map