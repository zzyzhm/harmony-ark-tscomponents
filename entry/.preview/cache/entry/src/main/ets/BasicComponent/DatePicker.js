"use strict";
class DatePickerExample extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.__isLunar = new ObservedPropertySimplePU(false, this, "isLunar");
        this.selectedDate = new Date('2021-08-08');
        this.__isMilitaryTime = new ObservedPropertySimplePU(false, this, "isMilitaryTime");
        this.selectedTime = new Date('2022-07-22T08:00:00');
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
        if (params.isLunar !== undefined) {
            this.isLunar = params.isLunar;
        }
        if (params.selectedDate !== undefined) {
            this.selectedDate = params.selectedDate;
        }
        if (params.isMilitaryTime !== undefined) {
            this.isMilitaryTime = params.isMilitaryTime;
        }
        if (params.selectedTime !== undefined) {
            this.selectedTime = params.selectedTime;
        }
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
        this.__isLunar.purgeDependencyOnElmtId(rmElmtId);
        this.__isMilitaryTime.purgeDependencyOnElmtId(rmElmtId);
    }
    aboutToBeDeleted() {
        this.__isLunar.aboutToBeDeleted();
        this.__isMilitaryTime.aboutToBeDeleted();
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    get isLunar() {
        return this.__isLunar.get();
    }
    set isLunar(newValue) {
        this.__isLunar.set(newValue);
    }
    get isMilitaryTime() {
        return this.__isMilitaryTime.get();
    }
    set isMilitaryTime(newValue) {
        this.__isMilitaryTime.set(newValue);
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("BasicComponent/DatePicker.ets(22:5)");
            Column.width('100%');
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Button.createWithLabel('切换公历农历');
            Button.debugLine("BasicComponent/DatePicker.ets(23:7)");
            Button.margin({ top: 30, bottom: 30 });
            Button.onClick(() => {
                this.isLunar = !this.isLunar;
            });
            if (!isInitialRender) {
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Button.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            DatePicker.create({
                start: new Date('2020-1-1'),
                end: new Date('2030-1-1'),
                selected: this.selectedDate
            });
            DatePicker.debugLine("BasicComponent/DatePicker.ets(28:7)");
            DatePicker.lunar(this.isLunar);
            DatePicker.onChange((value) => {
                this.selectedDate.setFullYear(value.year, value.month, value.day);
                console.info('select current date is: ' + JSON.stringify(value));
            });
            if (!isInitialRender) {
                DatePicker.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        DatePicker.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Blank.create(11);
            Blank.debugLine("BasicComponent/DatePicker.ets(42:7)");
            Blank.color(Color.Red);
            if (!isInitialRender) {
                Blank.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Blank.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Button.createWithLabel('切换12小时制/24小时制');
            Button.debugLine("BasicComponent/DatePicker.ets(44:7)");
            Button.margin({ top: 30, bottom: 30 });
            Button.onClick(() => {
                this.isMilitaryTime = !this.isMilitaryTime;
            });
            if (!isInitialRender) {
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Button.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            TimePicker.create({
                selected: this.selectedTime,
            });
            TimePicker.debugLine("BasicComponent/DatePicker.ets(49:7)");
            TimePicker.useMilitaryTime(this.isMilitaryTime);
            TimePicker.onChange((value) => {
                this.selectedTime.setHours(value.hour, value.minute);
                console.info('select current date is: ' + JSON.stringify(value));
            });
            if (!isInitialRender) {
                TimePicker.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        TimePicker.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Button.createWithLabel('获取日期和时间');
            Button.debugLine("BasicComponent/DatePicker.ets(58:7)");
            Button.onClick(() => {
                console.log('dateTime', this.selectedDate + '----' + this.selectedTime);
            });
            if (!isInitialRender) {
                Button.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Button.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new DatePickerExample(undefined, {}));
ViewStackProcessor.StopGetAccessRecording();
//# sourceMappingURL=DatePicker.js.map