"use strict";
class CheckboxExample extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("BasicComponent/Checkbox.ets(21:5)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Row.create();
            Row.debugLine("BasicComponent/Checkbox.ets(22:7)");
            if (!isInitialRender) {
                Row.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Checkbox.create({ name: 'checkbox1', group: 'checkboxGroup1' });
            Checkbox.debugLine("BasicComponent/Checkbox.ets(24:9)");
            Checkbox.select(true);
            Checkbox.selectedColor(0xed6f21);
            Checkbox.onChange((value) => {
                console.info('Checkbox1 change is' + value);
            });
            if (!isInitialRender) {
                Checkbox.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Checkbox.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Checkbox.create({ name: 'checkbox2', group: 'checkboxGroup1' });
            Checkbox.debugLine("BasicComponent/Checkbox.ets(31:9)");
            Checkbox.select(false);
            Checkbox.selectedColor(0x39a2db);
            Checkbox.onChange((value) => {
                console.info('Checkbox2 change is' + value);
            });
            if (!isInitialRender) {
                Checkbox.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Checkbox.pop();
        Row.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Blank.create(21);
            Blank.debugLine("BasicComponent/Checkbox.ets(40:7)");
            Blank.color(Color.Red);
            if (!isInitialRender) {
                Blank.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Blank.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Scroll.create();
            Scroll.debugLine("BasicComponent/Checkbox.ets(41:7)");
            if (!isInitialRender) {
                Scroll.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create();
            Column.debugLine("BasicComponent/Checkbox.ets(42:9)");
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // 全选按钮
            Flex.create({ justifyContent: FlexAlign.Start, alignItems: ItemAlign.Center });
            Flex.debugLine("BasicComponent/Checkbox.ets(44:11)");
            if (!isInitialRender) {
                // 全选按钮
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            CheckboxGroup.create({ group: 'checkboxGroup' });
            CheckboxGroup.debugLine("BasicComponent/Checkbox.ets(45:13)");
            CheckboxGroup.selectedColor('#007DFF');
            CheckboxGroup.onChange((itemName) => {
                console.info("checkbox group content" + JSON.stringify(itemName));
            });
            if (!isInitialRender) {
                CheckboxGroup.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        CheckboxGroup.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('Select All');
            Text.debugLine("BasicComponent/Checkbox.ets(50:13)");
            Text.fontSize(14);
            Text.lineHeight(20);
            Text.fontColor('#182431');
            Text.fontWeight(500);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        // 全选按钮
        Flex.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // 选项1
            Flex.create({ justifyContent: FlexAlign.Start, alignItems: ItemAlign.Center });
            Flex.debugLine("BasicComponent/Checkbox.ets(54:11)");
            // 选项1
            Flex.margin({ left: 36 });
            if (!isInitialRender) {
                // 选项1
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Checkbox.create({ name: 'checkbox1', group: 'checkboxGroup' });
            Checkbox.debugLine("BasicComponent/Checkbox.ets(55:13)");
            Checkbox.selectedColor('#007DFF');
            Checkbox.onChange((value) => {
                console.info('Checkbox1 change is' + value);
            });
            if (!isInitialRender) {
                Checkbox.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Checkbox.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('Checkbox1');
            Text.debugLine("BasicComponent/Checkbox.ets(60:13)");
            Text.fontSize(14);
            Text.lineHeight(20);
            Text.fontColor('#182431');
            Text.fontWeight(500);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        // 选项1
        Flex.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // 选项2
            Flex.create({ justifyContent: FlexAlign.Start, alignItems: ItemAlign.Center });
            Flex.debugLine("BasicComponent/Checkbox.ets(64:11)");
            // 选项2
            Flex.margin({ left: 36 });
            if (!isInitialRender) {
                // 选项2
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Checkbox.create({ name: 'checkbox2', group: 'checkboxGroup' });
            Checkbox.debugLine("BasicComponent/Checkbox.ets(65:13)");
            Checkbox.selectedColor('#007DFF');
            Checkbox.onChange((value) => {
                console.info('Checkbox2 change is' + value);
            });
            if (!isInitialRender) {
                Checkbox.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Checkbox.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('Checkbox2');
            Text.debugLine("BasicComponent/Checkbox.ets(70:13)");
            Text.fontSize(14);
            Text.lineHeight(20);
            Text.fontColor('#182431');
            Text.fontWeight(500);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        // 选项2
        Flex.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // 选项3
            Flex.create({ justifyContent: FlexAlign.Start, alignItems: ItemAlign.Center });
            Flex.debugLine("BasicComponent/Checkbox.ets(74:11)");
            // 选项3
            Flex.margin({ left: 36 });
            if (!isInitialRender) {
                // 选项3
                Flex.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Checkbox.create({ name: 'checkbox3', group: 'checkboxGroup' });
            Checkbox.debugLine("BasicComponent/Checkbox.ets(75:13)");
            Checkbox.selectedColor('#007DFF');
            Checkbox.onChange((value) => {
                console.info('Checkbox3 change is' + value);
            });
            if (!isInitialRender) {
                Checkbox.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Checkbox.pop();
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('Checkbox3');
            Text.debugLine("BasicComponent/Checkbox.ets(80:13)");
            Text.fontSize(14);
            Text.lineHeight(20);
            Text.fontColor('#182431');
            Text.fontWeight(500);
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        // 选项3
        Flex.pop();
        Column.pop();
        Scroll.pop();
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new CheckboxExample(undefined, {}));
ViewStackProcessor.StopGetAccessRecording();
//# sourceMappingURL=Checkbox.js.map