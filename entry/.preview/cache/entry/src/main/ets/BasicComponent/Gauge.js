"use strict";
class GaugeExample extends ViewPU {
    constructor(parent, params, __localStorage, elmtId = -1) {
        super(parent, __localStorage, elmtId);
        this.setInitiallyProvidedValue(params);
    }
    setInitiallyProvidedValue(params) {
    }
    updateStateVars(params) {
    }
    purgeVariableDependenciesOnElmtId(rmElmtId) {
    }
    aboutToBeDeleted() {
        SubscriberManager.Get().delete(this.id__());
        this.aboutToBeDeletedInternal();
    }
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Column.create({ space: 20 });
            Column.debugLine("BasicComponent/Gauge.ets(16:5)");
            Column.width('100%');
            Column.margin({ top: 5 });
            if (!isInitialRender) {
                Column.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // 使用默认的min和max为0-100，角度范围默认0-360，value值设置
            // 参数中设置当前值为75
            Gauge.create({ value: 75 });
            Gauge.debugLine("BasicComponent/Gauge.ets(20:7)");
            // 使用默认的min和max为0-100，角度范围默认0-360，value值设置
            // 参数中设置当前值为75
            Gauge.width(200);
            // 使用默认的min和max为0-100，角度范围默认0-360，value值设置
            // 参数中设置当前值为75
            Gauge.height(200);
            // 使用默认的min和max为0-100，角度范围默认0-360，value值设置
            // 参数中设置当前值为75
            Gauge.colors([[0x317AF7, 1], [0x5BA854, 1], [0xE08C3A, 1], [0x9C554B, 1]]);
            if (!isInitialRender) {
                // 使用默认的min和max为0-100，角度范围默认0-360，value值设置
                // 参数中设置当前值为75
                Gauge.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // 参数设置当前值为75，属性设置值为25，属性设置优先级高
            Gauge.create({ value: 75 });
            Gauge.debugLine("BasicComponent/Gauge.ets(25:7)");
            // 参数设置当前值为75，属性设置值为25，属性设置优先级高
            Gauge.value(25);
            // 参数设置当前值为75，属性设置值为25，属性设置优先级高
            Gauge.width(200);
            // 参数设置当前值为75，属性设置值为25，属性设置优先级高
            Gauge.height(200);
            // 参数设置当前值为75，属性设置值为25，属性设置优先级高
            Gauge.colors([[0x317AF7, 1], [0x5BA854, 1], [0xE08C3A, 1], [0x9C554B, 1]]);
            if (!isInitialRender) {
                // 参数设置当前值为75，属性设置值为25，属性设置优先级高
                Gauge.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            // 210--150度环形图表
            /**
             * startAngle:设置起始角度位置，时钟0点为0度，顺时针方向为正角度。       默认值：0
             * endAngle: 设置终止角度位置，时钟0点为0度，顺时针方向为正角度
             * colors:设置量规图的颜色，支持分段颜色设置。
             * strokeWidth:设置环形量规图的环形厚度。  默认值：4 单位：vp
             */
            Gauge.create({ value: 30, min: 0, max: 100 });
            Gauge.debugLine("BasicComponent/Gauge.ets(37:7)");
            // 210--150度环形图表
            /**
             * startAngle:设置起始角度位置，时钟0点为0度，顺时针方向为正角度。       默认值：0
             * endAngle: 设置终止角度位置，时钟0点为0度，顺时针方向为正角度
             * colors:设置量规图的颜色，支持分段颜色设置。
             * strokeWidth:设置环形量规图的环形厚度。  默认值：4 单位：vp
             */
            Gauge.startAngle(210);
            // 210--150度环形图表
            /**
             * startAngle:设置起始角度位置，时钟0点为0度，顺时针方向为正角度。       默认值：0
             * endAngle: 设置终止角度位置，时钟0点为0度，顺时针方向为正角度
             * colors:设置量规图的颜色，支持分段颜色设置。
             * strokeWidth:设置环形量规图的环形厚度。  默认值：4 单位：vp
             */
            Gauge.endAngle(150);
            // 210--150度环形图表
            /**
             * startAngle:设置起始角度位置，时钟0点为0度，顺时针方向为正角度。       默认值：0
             * endAngle: 设置终止角度位置，时钟0点为0度，顺时针方向为正角度
             * colors:设置量规图的颜色，支持分段颜色设置。
             * strokeWidth:设置环形量规图的环形厚度。  默认值：4 单位：vp
             */
            Gauge.colors([[0x317AF7, 0.1], [0x5BA854, 0.2], [0xE08C3A, 0.3], [0x9C554B, 0.4]]);
            // 210--150度环形图表
            /**
             * startAngle:设置起始角度位置，时钟0点为0度，顺时针方向为正角度。       默认值：0
             * endAngle: 设置终止角度位置，时钟0点为0度，顺时针方向为正角度
             * colors:设置量规图的颜色，支持分段颜色设置。
             * strokeWidth:设置环形量规图的环形厚度。  默认值：4 单位：vp
             */
            Gauge.strokeWidth(20);
            // 210--150度环形图表
            /**
             * startAngle:设置起始角度位置，时钟0点为0度，顺时针方向为正角度。       默认值：0
             * endAngle: 设置终止角度位置，时钟0点为0度，顺时针方向为正角度
             * colors:设置量规图的颜色，支持分段颜色设置。
             * strokeWidth:设置环形量规图的环形厚度。  默认值：4 单位：vp
             */
            Gauge.width(200);
            // 210--150度环形图表
            /**
             * startAngle:设置起始角度位置，时钟0点为0度，顺时针方向为正角度。       默认值：0
             * endAngle: 设置终止角度位置，时钟0点为0度，顺时针方向为正角度
             * colors:设置量规图的颜色，支持分段颜色设置。
             * strokeWidth:设置环形量规图的环形厚度。  默认值：4 单位：vp
             */
            Gauge.height(200);
            if (!isInitialRender) {
                // 210--150度环形图表
                /**
                 * startAngle:设置起始角度位置，时钟0点为0度，顺时针方向为正角度。       默认值：0
                 * endAngle: 设置终止角度位置，时钟0点为0度，顺时针方向为正角度
                 * colors:设置量规图的颜色，支持分段颜色设置。
                 * strokeWidth:设置环形量规图的环形厚度。  默认值：4 单位：vp
                 */
                Gauge.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Column.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
ViewStackProcessor.StartGetAccessRecordingFor(ViewStackProcessor.AllocateNewElmetIdForNextComponent());
loadDocument(new GaugeExample(undefined, {}));
ViewStackProcessor.StopGetAccessRecording();
//# sourceMappingURL=Gauge.js.map