/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import ItemData from '@bundle:com.example.component/entry/ets/viewmodel/ItemData';
/**
 * Binds data to components and provides interfaces.
 */
export class MainViewModel {
    /**
     * Get swiper image data.
     *
     * @return {Array<Resource>} swiperImages.
     */
    getSwiperImages() {
        let swiperImages = [
            { "id": 16777309, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" },
            { "id": 16777307, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" },
            { "id": 16777320, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" },
            { "id": 16777319, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" }
        ];
        return swiperImages;
    }
    /**
     * Get data of the first grid.
     *
     * @return {Array<PageResource>} firstGridData.
     */
    getFirstGridData() {
        let firstGridData = [
            new ItemData({ "id": 16777264, "type": 10003, params: [], "bundleName": "com.example.component", "moduleName": "entry" }, { "id": 16777311, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" }),
            new ItemData({ "id": 16777241, "type": 10003, params: [], "bundleName": "com.example.component", "moduleName": "entry" }, { "id": 16777315, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" }),
            new ItemData({ "id": 16777259, "type": 10003, params: [], "bundleName": "com.example.component", "moduleName": "entry" }, { "id": 16777223, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" }),
            new ItemData({ "id": 16777282, "type": 10003, params: [], "bundleName": "com.example.component", "moduleName": "entry" }, { "id": 16777308, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" }),
            new ItemData({ "id": 16777263, "type": 10003, params: [], "bundleName": "com.example.component", "moduleName": "entry" }, { "id": 16777222, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" }),
            new ItemData({ "id": 16777240, "type": 10003, params: [], "bundleName": "com.example.component", "moduleName": "entry" }, { "id": 16777219, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" }),
            new ItemData({ "id": 16777237, "type": 10003, params: [], "bundleName": "com.example.component", "moduleName": "entry" }, { "id": 16777306, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" }),
            new ItemData({ "id": 16777270, "type": 10003, params: [], "bundleName": "com.example.component", "moduleName": "entry" }, { "id": 16777313, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" })
        ];
        return firstGridData;
    }
    /**
     * Get data of the second grid.
     *
     * @return {Array<PageResource>} secondGridData.
     */
    getSecondGridData() {
        let secondGridData = [
            new ItemData({ "id": 16777257, "type": 10003, params: [], "bundleName": "com.example.component", "moduleName": "entry" }, { "id": 16777321, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" }, { "id": 16777256, "type": 10003, params: [], "bundleName": "com.example.component", "moduleName": "entry" }),
            new ItemData({ "id": 16777250, "type": 10003, params: [], "bundleName": "com.example.component", "moduleName": "entry" }, { "id": 16777310, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" }, { "id": 16777255, "type": 10003, params: [], "bundleName": "com.example.component", "moduleName": "entry" }),
            new ItemData({ "id": 16777248, "type": 10003, params: [], "bundleName": "com.example.component", "moduleName": "entry" }, { "id": 16777304, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" }, { "id": 16777253, "type": 10003, params: [], "bundleName": "com.example.component", "moduleName": "entry" }),
            new ItemData({ "id": 16777249, "type": 10003, params: [], "bundleName": "com.example.component", "moduleName": "entry" }, { "id": 16777316, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" }, { "id": 16777254, "type": 10003, params: [], "bundleName": "com.example.component", "moduleName": "entry" })
        ];
        return secondGridData;
    }
    /**
     * Get data of the setting list.
     *
     * @return {Array<PageResource>} settingListData.
     */
    getSettingListData() {
        let settingListData = [
            new ItemData({ "id": 16777278, "type": 10003, params: [], "bundleName": "com.example.component", "moduleName": "entry" }, { "id": 16777228, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" }, { "id": 16777281, "type": 10003, params: [], "bundleName": "com.example.component", "moduleName": "entry" }),
            new ItemData({ "id": 16777276, "type": 10003, params: [], "bundleName": "com.example.component", "moduleName": "entry" }, { "id": 16777220, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" }),
            new ItemData({ "id": 16777277, "type": 10003, params: [], "bundleName": "com.example.component", "moduleName": "entry" }, { "id": 16777314, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" }),
            new ItemData({ "id": 16777275, "type": 10003, params: [], "bundleName": "com.example.component", "moduleName": "entry" }, { "id": 16777324, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" }),
            new ItemData({ "id": 16777280, "type": 10003, params: [], "bundleName": "com.example.component", "moduleName": "entry" }, { "id": 16777224, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" }),
            new ItemData({ "id": 16777279, "type": 10003, params: [], "bundleName": "com.example.component", "moduleName": "entry" }, { "id": 16777225, "type": 20000, params: [], "bundleName": "com.example.component", "moduleName": "entry" })
        ];
        return settingListData;
    }
}
export default new MainViewModel();
//# sourceMappingURL=MainViewModel.js.map